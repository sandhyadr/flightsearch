package org.flight.search.Services;

import org.flight.search.Model.Flight;
import org.flight.search.Model.FlightSchedule;
import org.flight.search.Services.EconomyFareCalculation;
import org.flight.search.Services.BusinessFareCalculation;
import org.flight.search.Services.FirstClassFareCalculation;

import org.flight.search.Repository.FlightRepository;
import org.flight.search.ViewModel.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class FlightServicesTest {

    @Mock
    FlightRepository repository;
    private List<FlightSchedule> Flights;

    @Test
    public void shouldReturnNoFlightWhenSourceOrDestinationDoesNotMatch() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Hyderabad";
        criteria.destination = "Hyderabad";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println("Array l" + flt.size());
        flt.forEach(System.out::println);
        assertEquals(0, flt.size());

    }

    @Test
    public void shouldReturnFlightWhenSourceAndDestinationMatches() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Hyderabad";
        criteria.destination = "Banglore";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println("Array l" + flt.size());
        flt.forEach(System.out::println);
        assertEquals(0, flt.size());

    }

    @Test
    public void shouldReturnListWhenPassengerCountGivenAsZero() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Banglore";
        criteria.destination = "Hyderabad";
        criteria.passengerCount = 0;

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(1, flt.size());
    }

    @Test
    public void shouldReturnListWhenPassengerCountGivenIsLessThanAvailableSeats() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Delhi";
        criteria.destination = "Hyderabad";
        criteria.passengerCount = 2;

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(2, flt.size());

    }


    @Test
    public void shouldReturnListWhenDepartureDateMatchesFlightDate() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Banglore";
        criteria.destination = "Hyderabad";
        criteria.passengerCount = 0;
        criteria.departureDate = "2019-08-28";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(1, flt.size());
    }

    @Test
    public void shouldReturnListWhenDepartureDateNotGivenFlightDateIsNextDay() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Hyderabad";
        criteria.destination = "Banglore";
        criteria.passengerCount = 0;
        criteria.departureDate = "";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(1, flt.size());
    }

    @Test
    public void shouldReturnECInListWhenFlightClassIsNotGiven() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Hyderabad";
        criteria.destination = "Banglore";
        criteria.flightClass = "";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(1, flt.size());
    }

    @Test
    public void shouldReturnListWhenFlightClassGiven() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
        criteria.source = "Hyderabad";
        criteria.destination = "Banglore";
        criteria.flightClass = "EC";

        List<FlightSchedule> flt = flightServices.searchFlight(criteria);
        System.out.println(flt.size());
        System.out.println(flt);

        assertEquals(1, flt.size());
    }


    @Test
    public void shouldReturnTotalFareWhenFlightBusinessClassGiven() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();

        criteria.passengerCount = 5;
        criteria.departureDate = "2019-08-24";
      //double total = flightServices.BusinessFareCalculation(criteria);
       // System.out.println(total);

    }

    @Test
    public void shouldReturnTotalFareWhenFlightFirstClassGiven() throws ParseException {
        List<Flight> flights = new ArrayList<Flight>();

        FlightServices flightServices = new FlightServices(repository);

        Mockito.when(repository.getFlights()).thenReturn(flights);

        SearchCriteria criteria = new SearchCriteria();
       // criteria.baseFare=6000;
        criteria.passengerCount = 5;
        criteria.departureDate = "2019-08-24";
       // double total = flightServices.firstClassFareCalculation(criteria);
        //System.out.println(total);

    }

}