package org.flight.search.Repository;

import org.flight.search.Model.*;
import org.springframework.stereotype.Repository;
import static org.flight.search.Model.FlightClassType.*;
import java.util.*;


@Repository
public class FlightRepository {
     public void  FlightRepository() {}

   public  List<Flight> getFlights(){

        List<Flight>  flights = new ArrayList<Flight>();

        Map<FlightClassType, FlightSeatAllotment> flightClassAndSeats1 = new HashMap<>();
        Map<FlightClassType,FlightSeatAllotment> flightClassAndSeats2 = new HashMap<>();
        Map<FlightClassType,FlightSeatAllotment> flightClassAndSeats3 = new HashMap<>();

        FlightSeatAllotment fsa1 = new FlightSeatAllotment(1,1,EconomyClass,195);
        FlightSeatAllotment fsa2 = new FlightSeatAllotment(2,1,FirstClass,8);
        FlightSeatAllotment fsa3 = new FlightSeatAllotment(3,1,BusinessClass,35);


        flightClassAndSeats1.put(fsa1.getFlightClass(),fsa1);
        flightClassAndSeats1.put(fsa2.getFlightClass(),fsa2);
        flightClassAndSeats1.put(fsa3.getFlightClass(),fsa3);

        FlightSeatAllotment fsa4 = new FlightSeatAllotment(4,2,EconomyClass,144);
        FlightSeatAllotment fsa5 = new FlightSeatAllotment(5,2,FirstClass,0);
        FlightSeatAllotment fsa6 = new FlightSeatAllotment(6,2,BusinessClass,0);
        flightClassAndSeats2.put(fsa4.getFlightClass(),fsa4);
        flightClassAndSeats2.put(fsa5.getFlightClass(),fsa5);
        flightClassAndSeats2.put(fsa6.getFlightClass(),fsa6);


        FlightSeatAllotment fsa7 = new FlightSeatAllotment(7,3,EconomyClass,152);
        FlightSeatAllotment fsa8 = new FlightSeatAllotment(8,3,FirstClass,0);
        FlightSeatAllotment fsa9 = new FlightSeatAllotment(9,3,BusinessClass,20);
        flightClassAndSeats3.put(fsa7.getFlightClass(),fsa7);
        flightClassAndSeats3.put(fsa8.getFlightClass(),fsa8);
        flightClassAndSeats3.put(fsa9.getFlightClass(),fsa9);

        Flight fm1 = new Flight(1,"F0001","Boeing777", flightClassAndSeats1);
        Flight fm2 = new Flight(2,"F0002","Airbus319V2", flightClassAndSeats2);
        Flight fm3 = new Flight(3,"F0003","Airbus321", flightClassAndSeats3);
        flights.add(fm1);
        flights.add(fm2);
        flights.add(fm3);

        return flights;
}


     public List<FlightSchedule> getFlightSchedules()
     {
          Map<FlightClassType,FlightFares> fare1 = new HashMap<>();
          Map<FlightClassType,FlightFares> fare2 = new HashMap<>();
          Map<FlightClassType,FlightFares> fare3 = new HashMap<>();

          FlightFares fF1 = new FlightFares(1,1,FirstClass, 2,20000);
          FlightFares fF2 = new FlightFares(2,1,BusinessClass, 3,13000);
          FlightFares fF3 = new FlightFares(3,1,EconomyClass, 15,6000);
          FlightFares fF4 = new FlightFares(4,2,EconomyClass, 15,4000);
          FlightFares fF5 = new FlightFares(5,3,FirstClass, 2,20000);
          FlightFares fF6 = new FlightFares(6,3,BusinessClass, 2,10000);
          FlightFares fF7 = new FlightFares(7,3,EconomyClass, 10,5000);

          fare1.put(fF1.getFlightClass(),fF1);
          fare1.put(fF2.getFlightClass(),fF2);
          fare1.put(fF3.getFlightClass(),fF3);
          fare2.put(fF4.getFlightClass(),fF4);
          fare3.put(fF5.getFlightClass(),fF5);
          fare3.put(fF6.getFlightClass(),fF6);
          fare3.put(fF7.getFlightClass(),fF7);

          FlightSchedule fs1 = new FlightSchedule(1,1, "Hyderabad", "Delhi","2019-08-30",fare1);

          FlightSchedule fs2 = new FlightSchedule(2,1, "Delhi", "Hyderabad", "2019-08-29",fare1);

          FlightSchedule fs5 = new FlightSchedule(3,1, "Delhi", "Hyderabad", "2019-08-28",fare1);

          FlightSchedule fs3 = new FlightSchedule(4,2, "Banglore", "Hyderabad", "2019-08-28",fare2);

          FlightSchedule fs4 = new FlightSchedule(5,3, "Hyderabad", "Banglore", "2019-08-28",fare3);

          List<FlightSchedule> flightSchedules = new ArrayList<>();
          flightSchedules.add(fs1);

          flightSchedules.add(fs2);
          flightSchedules.add(fs3);
          flightSchedules.add(fs4);flightSchedules.add(fs5);


          return flightSchedules;
     }
     }

