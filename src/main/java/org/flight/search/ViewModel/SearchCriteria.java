package org.flight.search.ViewModel;

public class SearchCriteria {

   public String source;
    public String destination;
    public int passengerCount;
    public String departureDate;
    public String flightClass;

 public String getSource() {
  return source;
 }

 public void setSource(String source) {
  this.source = source;
 }

 public String getDestination() {
  return destination;
 }

 public void setDestination(String destination) {
  this.destination = destination;
 }

 public int getPassengerCount() {
  return passengerCount;
 }

 public void setPassengerCount(int passengerCount) {
  this.passengerCount = passengerCount;
 }

 public String getDepartureDate() {
  return departureDate;
 }

 public void setDepartureDate(String departureDate) {
  this.departureDate = departureDate;
 }

 public String getFlightClass() {
  return flightClass;
 }

 public void setFlightClass(String flightClass) {
  this.flightClass = flightClass;
 }
}
