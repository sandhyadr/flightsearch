package org.flight.search.controller;

import org.flight.search.Model.AjaxResponseBody;
import org.flight.search.Services.FlightServices;
import org.flight.search.ViewModel.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
public class FlightController {
    FlightServices flightServices;
    @Autowired
    public FlightController(FlightServices flightServices) {
        this.flightServices = flightServices;
    }

    @PostMapping  (value = "/searchFlight")
    public ResponseEntity searchFlight(@Valid @RequestBody SearchCriteria searchCriteria, Errors errors) throws ParseException {
        AjaxResponseBody ajaxresponse = new AjaxResponseBody();

        ajaxresponse.result = flightServices.searchFlight(searchCriteria);
        ajaxresponse.setResult(ajaxresponse.result);

        return ResponseEntity.ok(ajaxresponse);
    }

}
//@RestController
//public class FlightController {
//
//    @Autowired
//    FlightServices flightServices;
//
//    public FlightController(FlightServices flightServices) {
//        this.flightServices = flightServices;
//    }
//
//    @PostMapping(value="/searchFlight")
//    public ResponseEntity<?> searchResultViaAjax(@RequestBody SearchCriteria searchParams) {
//
//        AjaxResponseBody ajaxresponse = new AjaxResponseBody();
//        //If error, just return a 400 bad request, along with the error message
////            if (errors.hasErrors()) {
////
////                ajaxresponse.setMsg(errors.getAllErrors()
////                        .stream().map(x -> x.getDefaultMessage())
////                        .collect(Collectors.joining(",")));
////
////                ResponseEntity.badRequest().body(ajaxresponse);
////          }
//        ajaxresponse.result = flightServices.searchFlight(searchParams);
///*
//                   searchParams.source,
//                    searchParams.destination,
//                    searchParams.passengerCount,
//                    searchParams.departureDate,
//                    searchParams.baseFare,
//                    searchParams.flightClass
//            );*/
//        // System.out.println(ajaxresponse.result);
//        ajaxresponse.setResult(ajaxresponse.result);
//        return ResponseEntity.ok(ajaxresponse);
//    }
//
//
//}
//
//


