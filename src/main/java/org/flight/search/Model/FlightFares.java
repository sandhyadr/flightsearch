package org.flight.search.Model;

public class FlightFares {

    int id;
    private int routeId;
    private FlightClassType flightClass;
    private int availableSeats;
    private double baseFare;

    public FlightFares(int id, int routeId, FlightClassType flightClass, int availableSeats, double baseFare) {
        this.id = id;
        this.routeId = routeId;
        this.flightClass = flightClass;
        this.availableSeats = availableSeats;
        this.baseFare = baseFare;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public FlightClassType getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(FlightClassType flightClass) {
        this.flightClass = flightClass;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }
}