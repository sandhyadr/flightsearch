package org.flight.search.Model;
import org.flight.search.Model.FlightClassType;

public class FlightSeatAllotment {

    private int id;
    private int flightId;
    private FlightClassType flightClass;
     private   int    seatsAllotedForClass;

    public FlightSeatAllotment(){}

    public FlightSeatAllotment(int id, int flightId, FlightClassType flightClass, int seatsAllotedForClass) {
        this.id = id;
        this.flightId = flightId;
        this.flightClass = flightClass;
        this.seatsAllotedForClass = seatsAllotedForClass;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public FlightClassType getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(FlightClassType flightClass) {
        this.flightClass = flightClass;
    }

    public int getSeatsAllotedForClass() {
        return seatsAllotedForClass;
    }

    public void setSeatsAllotedForClass(int seatsAllotedForClass) {
        this.seatsAllotedForClass = seatsAllotedForClass;
    }
}

