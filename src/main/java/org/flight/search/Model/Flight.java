package org.flight.search.Model;

import java.util.*;

public class Flight {

    int flightId;
    String flightName;
    String flightCarrier;
  Map<FlightClassType,FlightSeatAllotment> flightClasses;

    public Flight(){}

    public Flight(int flightId, String flightName, String flightCarrier, Map<FlightClassType, FlightSeatAllotment> flightClasses) {
        this.flightId = flightId;
        this.flightName = flightName;
        this.flightCarrier = flightCarrier;
        this.flightClasses = flightClasses;
    }


    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightCarrier() {
        return flightCarrier;
    }

    public void setFlightCarrier(String flightCarrier) {
        this.flightCarrier = flightCarrier;
    }

    public Map<FlightClassType, FlightSeatAllotment> getFlightClasses() {
        return flightClasses;
    }

    public void setFlightClasses(Map<FlightClassType, FlightSeatAllotment> flightClasses) {
        this.flightClasses = flightClasses;
    }
}