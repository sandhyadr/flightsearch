package org.flight.search.Model;

import java.util.List;

public class AjaxResponseBody {

        public String msg;
        public List<FlightSchedule> result;


        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public List<FlightSchedule> getResult() {
            return result;
        }

        public void setResult(List<FlightSchedule> result) {
            this.result = result;
        }

    }

