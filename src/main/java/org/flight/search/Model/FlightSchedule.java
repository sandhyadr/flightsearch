package org.flight.search.Model;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.flight.search.Model.FlightFares;
import org.flight.search.Model.FlightClassType;

public class FlightSchedule {

    int id;
    int flightId;
    String source;
    String destination;
    String departureDate;
    Map<FlightClassType, FlightFares> flightFares;


    public FlightSchedule() {
    }

    public FlightSchedule(int id, int flightId, String source, String destination, String departureDate,
                          Map<FlightClassType, FlightFares> flighFares) {
        this.id = id;
        this.flightId = flightId;
        this.source = source;
        this.destination = destination;
        this.departureDate = departureDate;
        this.flightFares = flightFares;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public Map<FlightClassType, FlightFares> getFlightFares() {
        return flightFares;
    }

    public void setFlightFares(Map<FlightClassType, FlightFares> flightFares) {
        this.flightFares = flightFares;
    }
}