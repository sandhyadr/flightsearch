package org.flight.search.Services;

import org.flight.search.Model.*;
import org.flight.search.Repository.FlightRepository;
import org.flight.search.ViewModel.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.flight.search.Services.EconomyFareCalculation;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class FlightServices {


    @Autowired
    FlightRepository repository;

    public FlightServices(FlightRepository repository) {
        this.repository = repository;
    }


    public List<FlightSchedule> searchFlight(SearchCriteria searchParams) throws ParseException {

        searchParams = checkAndFixSearchParams(searchParams);
        //    System.out.println("S: " + searchParams.source + " D :" + searchParams.destination + " pc: " + searchParams.passengerCount);

        //  System.out.println("Heklllooooooooo");

        repository = new FlightRepository();
        List<Flight> flights = repository.getFlights();
        //  flights.forEach(System.out::println);
        List<FlightSchedule> flightSchedules = repository.getFlightSchedules();

        List<FlightSchedule> resultOfschedules = new ArrayList<>();

        Map<FlightClassType, FlightSeatAllotment> flightSeatAllotment;

        Map<FlightClassType, FlightFares> fare;

        int flightId = 0;
        double basefare = 0.0, totalFare = 0.0;
        int totalSeatsinClass = 0;

        for (FlightSchedule fs1 : flightSchedules) {
            if ((fs1.getSource().equals(searchParams.source)) &&
                    (fs1.getDestination().equals(searchParams.destination))
                    && (fs1.getDepartureDate().equals(searchParams.departureDate))) {

                flightId = fs1.getFlightId();
                //System.out.println("flightId is " + flightId);

                fare = fs1.getFlightFares();
                if ((fare.get(FlightClassType.EconomyClass).getFlightClass()).equals(searchParams.getFlightClass())) {
                    if ((fare.get(FlightClassType.EconomyClass).getRouteId() == flightId) &&
                            (fare.get(FlightClassType.EconomyClass).getAvailableSeats() >= searchParams.getPassengerCount())) {

                        basefare = fare.get(FlightClassType.EconomyClass).getBaseFare();

                    }

                } else if ((fare.get(FlightClassType.BusinessClass).getFlightClass()).equals(searchParams.getFlightClass())) {
                    if ((fare.get(FlightClassType.BusinessClass).getRouteId() == flightId) &&
                            (fare.get(FlightClassType.BusinessClass).getAvailableSeats() >= searchParams.getPassengerCount())) {

                        basefare = fare.get(FlightClassType.BusinessClass).getBaseFare();

                    }
                } else if ((fare.get(FlightClassType.FirstClass).getFlightClass()).equals(searchParams.getFlightClass())) {
                    if ((fare.get(FlightClassType.FirstClass).getRouteId() == flightId) &&
                            (fare.get(FlightClassType.FirstClass).getAvailableSeats() >= searchParams.getPassengerCount())) {

                        basefare = fare.get(FlightClassType.FirstClass).getBaseFare();

                    }
                }

                for (Flight f : flights) {
                    flightSeatAllotment = f.getFlightClasses();
                    if ((flightSeatAllotment.get(FlightClassType.EconomyClass).getFlightClass()).equals(searchParams.getFlightClass())) {
                        if ((flightSeatAllotment.get(FlightClassType.EconomyClass).getFlightId() == flightId)
                                && (flightSeatAllotment.get(FlightClassType.EconomyClass).getSeatsAllotedForClass() >= searchParams.getPassengerCount())) {

                            totalSeatsinClass = flightSeatAllotment.get(FlightClassType.EconomyClass).getSeatsAllotedForClass();
                            resultOfschedules.add(fs1);
                            System.out.println("seat alltment");
                        }
                    }

                }
            }
          totalFare=  calculateTotalFare(searchParams, basefare, totalSeatsinClass);
            System.out.println("totalFsre is :"+totalFare);
        }
        return resultOfschedules;
    }



   public double calculateTotalFare(SearchCriteria searchParams,double basefare, int totalSeatsinClass) throws ParseException {
        double totalFare=0.0;
        EconomyFareCalculation economyFareCalculation = new EconomyFareCalculation();
        FirstClassFareCalculation  firstClassFareCalculation=new FirstClassFareCalculation();
        BusinessFareCalculation businessFareCalculation =new BusinessFareCalculation();

        if(searchParams.getFlightClass() == "EconomyClass") {
            totalFare = economyFareCalculation.economyClassFareCalcuation(searchParams, basefare, totalSeatsinClass);
        }
        else if(searchParams.getFlightClass() == "FirstClass")
         {
        totalFare=firstClassFareCalculation.firstClassFareCalculation(searchParams, basefare);
         }
        else if (searchParams.getFlightClass() == "BusinessClass")
        {
            totalFare=businessFareCalculation.businessClassFareCalculation(searchParams,basefare);
        }
        return totalFare;
    }

    public SearchCriteria checkAndFixSearchParams(SearchCriteria searchParams) {
        SearchCriteria updateParams;
        if (searchParams.passengerCount == 0) {
            searchParams.passengerCount = 1;
            System.out.println("passengercount is : " + searchParams.passengerCount);
        }

        if (isNullOrEmpty(searchParams.departureDate)) {
            // DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localeDate = LocalDate.now();
            searchParams.departureDate = String.valueOf(localeDate.plusDays(1));
            System.out.println("date after today is : " + searchParams.departureDate);

        }

        if (isNullOrEmpty(searchParams.flightClass)) {
            System.out.println(" class selected is :" + searchParams.flightClass);
            searchParams.flightClass = "EconomyClass";
            System.out.println(" class selected is :" + searchParams.flightClass);
        }

        return searchParams;
    }


    public static boolean isNullOrEmpty(String param) {
        if (param != null && !param.trim().isEmpty())
            return false;
        else
            return true;


    }

}