package org.flight.search.Services;

import org.flight.search.Model.Flight;
import org.flight.search.Model.FlightClassType;
import org.flight.search.Model.FlightFares;
import org.flight.search.Model.FlightSchedule;
import org.flight.search.ViewModel.SearchCriteria;

public class EconomyFareCalculation {
    public double economyClassFareCalcuation(SearchCriteria searchParams,double basefare,int totalSeats) {

        Flight flight = new Flight();
        FlightSchedule fs = new FlightSchedule();

        int seats_to_be_booked = searchParams.passengerCount;

        int remainingSeats = fs.getFlightFares().get(FlightClassType.EconomyClass).getAvailableSeats();

        double totalFare = 0;
        int first_40percent_of_seats = (int) (totalSeats * 0.4f);
        int next_50percent_of_seats = (int) (totalSeats * 0.5f);
        int last_10percent_of_seats = totalSeats - (first_40percent_of_seats + next_50percent_of_seats);
        if (remainingSeats >= seats_to_be_booked) {
            int seats_tobe_booked_in_firstslot = remainingSeats - (next_50percent_of_seats + last_10percent_of_seats);
            if (seats_tobe_booked_in_firstslot > 0) {
                //
                totalFare += seats_tobe_booked_in_firstslot * basefare;
                remainingSeats = remainingSeats - seats_tobe_booked_in_firstslot;
                int seats_tobe_booked_in_second_and_third_slot = seats_to_be_booked - seats_tobe_booked_in_firstslot;
                if (seats_tobe_booked_in_second_and_third_slot <= next_50percent_of_seats) {
                    totalFare += seats_tobe_booked_in_second_and_third_slot * (basefare * 1.3f);
                } else {
                    int seats_in_second_slot = next_50percent_of_seats;
                    int seats_in_third_slot = seats_tobe_booked_in_second_and_third_slot - next_50percent_of_seats;
                    totalFare += seats_in_second_slot * (basefare * 1.3f);
                    totalFare += seats_in_third_slot * (basefare * 1.6f);
                }
            }
        }
  return totalFare;
    }
}