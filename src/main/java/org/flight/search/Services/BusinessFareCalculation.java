package org.flight.search.Services;

import org.flight.search.Model.FlightFares;
import org.flight.search.Model.FlightSchedule;
import org.flight.search.ViewModel.SearchCriteria;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BusinessFareCalculation {
    public double businessClassFareCalculation(SearchCriteria searchParams,double baseFare) throws ParseException {
        // 40% of base price on monday,friday,sunday

        Date date1=new SimpleDateFormat("yyyy-mm-dd").parse(searchParams.departureDate);

        SimpleDateFormat dayName = new SimpleDateFormat("EEEE");

        String day=dayName.format(date1);

        if ((day.equals("Monday")) || (day.equals("Friday")) || (day.equals("Sunday")))
        {

          baseFare = (baseFare * 1.4f);
        }
        double  totalFare = searchParams.passengerCount * baseFare;

        return Math.round(totalFare);
    }

}
