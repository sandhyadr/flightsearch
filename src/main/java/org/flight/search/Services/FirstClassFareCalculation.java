package org.flight.search.Services;

import org.flight.search.Model.FlightFares;
import org.flight.search.Model.FlightSchedule;
import org.flight.search.ViewModel.SearchCriteria;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class FirstClassFareCalculation {
    public double firstClassFareCalculation(SearchCriteria searchParams,double baseFare) throws ParseException
    {
//10% of baseprice starting from 10 days


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//todays date to book
        LocalDate bookingDate = LocalDate.now();
        //System.out.println(bookingDate);

        LocalDate departDate= LocalDate.parse(searchParams.departureDate,formatter);

        Period period = Period.between(departDate, bookingDate);
        if((period.getDays() <= 10)&& (period.getDays() >0))
        {
            int diff = period.getDays();
            //  System.out.println("diff = " + diff);
            baseFare = baseFare * diff * 0.1;

        }
        double  totalFare = searchParams.passengerCount * baseFare ;
        // System.out.println("totalfare "+totalFare);
        return Math.round(totalFare);
    }
}
