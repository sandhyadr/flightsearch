$(document).ready(function () {

    $("#flightsearchform").submit(function (event) {

        event.preventDefault();

        fire_ajax_submit();
    });
});


function fire_ajax_submit() {
    var search = {}
    search["source"] = $("#txtFrom").val();
    search["destination"] = $("#txtTo").val();
    search["passengerCount"] = $("#txtPasseng").val();
    search["departureDate"] = $("#txtDepDate").val();
    search["flightClass"] = $("#txtFlightClass").val();


    $("#btnSubmit").prop("disabled", true);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/searchFlight",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').html(json);

            console.log("SUCCESS : ", data);
            $("#btnSubmit").prop("disabled", false);
        }
        //,
        // error: function (e) {​
        //     var json = "<h4>Ajax Response</h4><pre>"
        //         + e.responseText + "</pre>";
        //     $('#feedback').html(json);
        //     console.log("ERROR : ", e);
        //     $("#btnSubmit").prop("disabled", false);
        // }
    });
}